﻿# GAMEJOLT API SAMPLE
# Made by Max le Fou - Released under LGPL
# File py_gjapi.rpy originally made by Vinicius Epiplon
# Modified by Max le Fou for Ren'Py compatibility.
# See the license.txt file to know more about it.

# The usual stuff set in Ren'Py games
init -1 python hide:
    
    # This needs to be here to remove that hideous default checkered background
    config.layers.insert(0, 'background')
    
    config.developer = True
    config.screen_width = 800
    config.screen_height = 600
    config.window_title = u"GameJolt API sample"
    config.name = "GameJolt API sample"
    config.version = "1.0"
    theme.threeD(
        widget = "#ad8c31",
        widget_hover = "#568153",
        widget_text = "#ffffff",
        widget_selected = "#f2edc4",
        disabled = "#12612f",
        disabled_text = "#2c6e44",
        label = "#ffffff",
        frame = "#00551f",
        mm_root = "title.png",
        gm_root = "#6b4a27",
        rounded_window = False,
        )

    config.has_sound = True
    config.has_music = True
    config.has_voice = False
    config.help = "README.md"

    config.enter_transition = None
    config.exit_transition = None
    config.intra_transition = None
    config.main_game_transition = None
    config.game_main_transition = None
    config.end_splash_transition = None
    config.end_game_transition = None
    config.after_load_transition = None
    config.window_show_transition = None
    config.window_hide_transition = None
    config.adv_nvl_transition = dissolve
    config.nvl_adv_transition = dissolve
    config.enter_yesno_transition = None
    config.exit_yesno_transition = None
    config.enter_replay_transition = None
    config.exit_replay_transition = None
    config.say_attribute_transition = None

python early:
    config.save_directory = "GameJolt API sample-1455812821"

init -1 python hide:
    config.default_fullscreen = False
    config.default_text_cps = 0
    config.default_afm_time = 10
    
########################################################################
# GAMEJOLT API

    # Define here the keys of the game. Don't forget this !!!
    persistent.GJGameKey = "127112"
    persistent.GJPrivateKey = "2c5425f2ed988824126d7d30d01da133"
    
    # Persistent values so they can be saved for later.
    persistent.GJusername
    persistent.GJtoken
    if persistent.GJstatus is None:
        persistent.GJstatus = "disconnected"
        
    
########################################################################

init python:
    build.directory_name = "GJAPI_sample"
    build.executable_name = "GJAPI"
    build.include_update = False
    build.classify('**~', None)
    build.classify('**.bak', None)
    build.classify('**.doc', None)
    build.classify('**/.**', None)
    build.classify('**/#**', None)
    build.classify('**/thumbs.db', None)
    build.documentation('*.md')
    

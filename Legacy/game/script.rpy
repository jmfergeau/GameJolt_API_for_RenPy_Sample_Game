﻿# GAMEJOLT API SAMPLE
# Made by Max le Fou - Released under LGPL3
# File py_gjapi.py originally made by Vinicius Epiplon
# Modified by Max le Fou for Ren'Py compatibility.

# WARNING!!!!!!
# You MUST have the file py_gjapi.rpy in the game folder in order to make this to work.
# You can find the original source of this file in the url below:
# https://github.com/maxlefou/gjapi_renpy

# Declare images below this line, using the image statement.
# eg. image eileen happy = "eileen_happy.png"

# The plain black background we want to set by default must be defined
image blackbg = "#000"

image bg = "bg whitehouse.jpg"
image eileen happy = "eileen happy.png"
image eileen concerned = "eileen concerned.png"
image eileen vhappy = "eileen vhappy.png"

# Declare characters used by this game.
define e = Character('Eileen', color="#c8ffc8")

# Don't touch this but include it.
define GJusername = None
define GJtoken = None
define GJscore = 0
define GJcheck1 = None
define GJcheck2 = None

# We use a splash screen to set up the Game Jolt connection variable
label splashscreen:
    # This needs to be here because renpy wants to put checkers instead of plain black
    scene blackbg onlayer background
    
    # Setting the init.
    $ GJset = GameJoltTrophy(persistent.GJusername, persistent.GJtoken, persistent.GJGameKey, persistent.GJPrivateKey)
    
    
    
    # Checking if a user is registered in the game. If yes, it opens the session and sets a readable variable
    if persistent.GJusername != None:
        python:    
            GJcheck2 = GJset.openSession()
        if GJcheck2 == True:
            $ GJusername = persistent.GJusername
    return

#########################################################
# Game Jolt connection screen
#
# This is where the user can connect or disconnect from Game Jolt

label gjconnect:
    # This needs to be here because renpy wants to put checkers instead of plain black
    scene blackbg onlayer background
    
    # If user is already connected, it asks if they want to logout.
    if persistent.GJusername != None:
        "Disconnect from Game Jolt?"
        menu:
                "Yes, disconnect.":
                    # Closes the session
                    python:
                        GJset.closeSession()
                        
                    # Cleaning persistent data
                    $ persistent.GJstatus = "disconnected"
                    $ persistent.GJusername = None
                    $ persistent.GJtoken = None
                    
                    # Renpy is too stupid to save these persistent data automatically so we must tell it to do it.
                    $ renpy.save_persistent()
                    
                    "Disconnected."
                    
                    # Returning to main menu. For some reason, renpy.MainMenu doesn't work anymore so we use this.
                    $ renpy.full_restart(transition=False, label='_invoke_main_menu', target='_main_menu')
                "No, return to menu.":
                    $ renpy.full_restart(transition=False, label='_invoke_main_menu', target='_main_menu')
    
    # If no user is connected, going to connect them
    else:
        
        # Asking for the username and token. Here, we use renpy.input, but other methods can be used.
        $ persistent.GJusername = renpy.input("Type here your username and press Enter.", "")
        $ persistent.GJtoken = renpy.input("Now, type here your game token and press Enter.", "")
        
        # Setting the init. For some random reason, this must be right here too.
        $ GJset = GameJoltTrophy(persistent.GJusername, persistent.GJtoken, persistent.GJGameKey, persistent.GJPrivateKey)
    
        python:
            GJset.changeUsername(persistent.GJusername)
            GJset.changeUserToken(persistent.GJtoken)
            GJcheck1 = GJset.authenticateUser()

        if GJcheck1 == True:
            # Authentication succesful, opening session.
            python:    
                GJcheck2 = GJset.openSession()
                
            # Session succesfully opened
            if GJcheck2 == True:
                "Connection to Game Jolt succesful."
                
                # Says to the game that it's connected
                $ persistent.GJstatus = "connected"
                
                # Renpy is too stupid to save these persistent data automatically so we must tell it to do it.
                $ renpy.save_persistent() 
                
                # Returning to main menu. For some reason, renpy.MainMenu doesn't work anymore so we use this.
                $ renpy.full_restart(transition=False, label='_invoke_main_menu', target='_main_menu')
            
            # If session couldn't open
            else:
                "A problem occured. Maybe your connection has a problem. Or maybe it's Game Jolt derping..."
                "Try again?"
                menu:
                    # Retry to connect
                    "Yes, try again.":
                        jump gjconnect
                        
                    # Give up
                    "No, return to menu.":
                        $ renpy.full_restart(transition=False, label='_invoke_main_menu', target='_main_menu')

        # If authentication failed
        else:
            "It seems the authentication failed. Maybe you didn't write correctly the username and token..."
            "Try again?"
            menu:
                # Retry to connect
                "Yes, try again.":
                    jump gjconnect
                    
                # Give up    
                "No, return to menu.":
                    $ renpy.full_restart(transition=False, label='_invoke_main_menu', target='_main_menu')

# The game starts here.
label start:
    # This needs to be here because renpy wants to put checkers instead of plain black
    scene blackbg onlayer background
    
    # Setting the init. For some random reason, this must be right here too.
    $ GJset = GameJoltTrophy(persistent.GJusername, persistent.GJtoken, persistent.GJGameKey, persistent.GJPrivateKey)
    
    show bg
    show eileen happy
    with fade
    e "Welcome to this sample of GameJolt API for Ren'Py."
    e "This little game will let you test the API and its sources will tell you how to use them!"
    
    python:    
        GJcheck2 = GJset.openSession()
    if GJcheck2 == True:
        $ GJusername = persistent.GJusername
        e "I see you're already connected as %(GJusername)s. Awesome ! Let's move on."
        jump TrophyTest
    else:
        e "I see you are not yet connected to Game Jolt. It's pretty pointless to play this demo if you are not connected to it. :p"
        e "You can connect to Game Jolt by going to the main menu and click on the Game Jolt icon at the bottom left of the screen."
        e "If you don't have a Game Jolt account,... Well, what are you waiting for? It's free and Game Jolt is a great platform! Go see for yourself at http://gamejolt.com"
        e "Once you are connected to Game Jolt, you should see your username near the Game Jolt icon. If you see it, start a new game."
        return

            
label TrophyTest:
    show eileen happy
    e "Now that we are connected, Let's celebrate this... With a trophy!"
    e "Here's your test trophy!"
    
    # Here's how to add a trophy to the user. Note that the trophy id must be put between the () in the GJset.addAchieved() command
    # Note that we also reopen the session. The session closes itself after 120 seconds of inactivity. This can be prevented with
    # the pingSession() command, but I haven't found the way to use it in a loop. (Trying to put a python loop in renpy seems to
    # destroy the game) If you have any idea of how to use pingSession in renpy, it's very welcomed!
    python:    
        GJcheck2 = GJset.openSession()
    if GJcheck2 == True:
        python:
            GJset.addAchieved(51027)
    
        show eileen vhappy
        e "Congratulations!"
        
    # In case the trophy couldn't be sent. This shouldn't happen unless you got disconnected during the game.
    else:
        e "An error has occured. Is internet still working?"
    
    show eileen happy
    e "Now let's see how the scoring works."
    e "I'm going to give you... let's see.... 1000 points."
    
    $ GJscore += 1000
    
    e "I'm now gonna generate a random number for more variety."
    
    $ GJscore += renpy.random.randint(500, 10000)
    
    e "You got a score of %(GJscore)s!"
    
    # Just for the lols, not needed. :p
    if GJscore >= 9000:
        show eileen vhappy
        "IT'S OVER NIIIIINE THOUUUUSAAAAAAAND!!!!!111one"
        show eileen happy
        
    e "Let's send this to GameJolt, shall we?"
    
    # Here's how to send a score to GameJolt. The score itself must be put in the first and the second definition (GJscore here)
    # Table_id is where you put the table id. The rest depends of your table and game's configurations
    python:    
        GJcheck2 = GJset.openSession()
    if GJcheck2 == True:
        python:
            GJset.addScores(GJscore, GJscore, table_id=131441, extra_data='', guest=False, guestname='')
    
        show eileen vhappy
        e "There we go!"
        
    # In case the trophy couldn't be sent. This shouldn't happen unless you got disconnected during the game.
    else:
        e "An error has occured. Is internet still working?"
        
    show eileen happy
    e "We can also use the storage data functions, but since it's for more advanced modding, I'll let you see more about it by looking at the source codes."
    e "This is the end of the demo. If you wish to logout from GameJolt, you can click on the Game Jolt icon in the main menu."
    e "Bye and see you around!"
    hide eileen
    hide bg
    with fade
    return
